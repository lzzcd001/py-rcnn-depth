name: "CaffeNet"
layer {
  name: "data_rgb"
  type: "WindowData"
  top: "data_rgb"
  top: "label_rgb"
  window_data_param {
    source: "WINDOW_FILE_TRAIN_COLOR"
    root_folder: SOURCE_FOLDER
    crop_mode: "warp"
    batch_size: 128
    fg_threshold: 0.5
    bg_threshold: 0.5
    fg_fraction: 0.25
    context_pad: 16
  }
  transform_param {
    crop_size: 227
    mirror: true
    mean_file: "MEAN_FILE_COLOR"
  }
}
layer {
  name: "conv1_rgb"
  type: "Convolution"
  bottom: "data_rgb"
  top: "conv1_rgb"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 96
    kernel_size: 11
    stride: 4
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "relu1_rgb"
  type: "ReLU"
  bottom: "conv1_rgb"
  top: "conv1_rgb"
}
layer {
  name: "pool1_rgb"
  type: "Pooling"
  bottom: "conv1_rgb"
  top: "pool1_rgb"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "norm1_rgb"
  type: "LRN"
  bottom: "pool1_rgb"
  top: "norm1_rgb"
  lrn_param {
    local_size: 5
    alpha: 0.0001
    beta: 0.75
  }
}
layer {
  name: "conv2_rgb"
  type: "Convolution"
  bottom: "norm1_rgb"
  top: "conv2_rgb"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 256
    pad: 2
    kernel_size: 5
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu2"
  type: "ReLU"
  bottom: "conv2_rgb"
  top: "conv2_rgb"
}
layer {
  name: "pool2_rgb"
  type: "Pooling"
  bottom: "conv2_rgb"
  top: "pool2_rgb"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "norm2_rgb"
  type: "LRN"
  bottom: "pool2_rgb"
  top: "norm2_rgb"
  lrn_param {
    local_size: 5
    alpha: 0.0001
    beta: 0.75
  }
}
layer {
  name: "conv3_rgb"
  type: "Convolution"
  bottom: "norm2_rgb"
  top: "conv3_rgb"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 384
    pad: 1
    kernel_size: 3
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "relu3_rgb"
  type: "ReLU"
  bottom: "conv3_rgb"
  top: "conv3_rgb"
}
layer {
  name: "conv4_rgb"
  type: "Convolution"
  bottom: "conv3_rgb"
  top: "conv4_rgb"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 384
    pad: 1
    kernel_size: 3
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu4_rgb"
  type: "ReLU"
  bottom: "conv4_rgb"
  top: "conv4_rgb"
}
layer {
  name: "conv5_rgb"
  type: "Convolution"
  bottom: "conv4_rgb"
  top: "conv5_rgb"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 256
    pad: 1
    kernel_size: 3
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu5_rgb"
  type: "ReLU"
  bottom: "conv5_rgb"
  top: "conv5_rgb"
}
layer {
  name: "pool5_rgb"
  type: "Pooling"
  bottom: "conv5_rgb"
  top: "pool5_rgb"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "fc6_rgb"
  type: "InnerProduct"
  bottom: "pool5_rgb"
  top: "fc6_rgb"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 4096
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu6_rgb"
  type: "ReLU"
  bottom: "fc6_rgb"
  top: "fc6_rgb"
}
layer {
  name: "drop6_rgb"
  type: "Dropout"
  bottom: "fc6_rgb"
  top: "fc6_rgb"
  dropout_param {
    dropout_ratio: 0.5
  }
}
layer {
  name: "fc7_rgb"
  type: "InnerProduct"
  bottom: "fc6_rgb"
  top: "fc7_rgb"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 4096
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu7_rgb"
  type: "ReLU"
  bottom: "fc7_rgb"
  top: "fc7_rgb"
}
layer {
  name: "drop7_rgb"
  type: "Dropout"
  bottom: "fc7_rgb"
  top: "fc7_rgb"
  dropout_param {
    dropout_ratio: 0.5
  }
}

# ================================
layer {
  name: "data_hha"
  type: "WindowData"
  top: "data_hha"
  top: "label_hha"
  window_data_param {
    source: "WINDOW_FILE_TRAIN_HHA"
    root_folder: SOURCE_FOLDER
    crop_mode: "warp"
    batch_size: 128
    fg_threshold: 0.5
    bg_threshold: 0.5
    fg_fraction: 0.25
    context_pad: 16
  }
  transform_param {
    crop_size: 227
    mirror: true
    mean_file: "MEAN_FILE_HHA"
  }
}
layer {
  name: "conv1_hha"
  type: "Convolution"
  bottom: "data_hha"
  top: "conv1_hha"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 96
    kernel_size: 11
    stride: 4
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "relu1_hha"
  type: "ReLU"
  bottom: "conv1_hha"
  top: "conv1_hha"
}
layer {
  name: "pool1_hha"
  type: "Pooling"
  bottom: "conv1_hha"
  top: "pool1_hha"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "norm1_hha"
  type: "LRN"
  bottom: "pool1_hha"
  top: "norm1_hha"
  lrn_param {
    local_size: 5
    alpha: 0.0001
    beta: 0.75
  }
}
layer {
  name: "conv2_hha"
  type: "Convolution"
  bottom: "norm1_hha"
  top: "conv2_hha"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 256
    pad: 2
    kernel_size: 5
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu2"
  type: "ReLU"
  bottom: "conv2_hha"
  top: "conv2_hha"
}
layer {
  name: "pool2_hha"
  type: "Pooling"
  bottom: "conv2_hha"
  top: "pool2_hha"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "norm2_hha"
  type: "LRN"
  bottom: "pool2_hha"
  top: "norm2_hha"
  lrn_param {
    local_size: 5
    alpha: 0.0001
    beta: 0.75
  }
}
layer {
  name: "conv3_hha"
  type: "Convolution"
  bottom: "norm2_hha"
  top: "conv3_hha"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 384
    pad: 1
    kernel_size: 3
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "relu3_hha"
  type: "ReLU"
  bottom: "conv3_hha"
  top: "conv3_hha"
}
layer {
  name: "conv4_hha"
  type: "Convolution"
  bottom: "conv3_hha"
  top: "conv4_hha"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 384
    pad: 1
    kernel_size: 3
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu4_hha"
  type: "ReLU"
  bottom: "conv4_hha"
  top: "conv4_hha"
}
layer {
  name: "conv5_hha"
  type: "Convolution"
  bottom: "conv4_hha"
  top: "conv5_hha"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 256
    pad: 1
    kernel_size: 3
    group: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu5_hha"
  type: "ReLU"
  bottom: "conv5_hha"
  top: "conv5_hha"
}
layer {
  name: "pool5_hha"
  type: "Pooling"
  bottom: "conv5_hha"
  top: "pool5_hha"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "fc6_hha"
  type: "InnerProduct"
  bottom: "pool5_hha"
  top: "fc6_hha"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 4096
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu6_hha"
  type: "ReLU"
  bottom: "fc6_hha"
  top: "fc6_hha"
}
layer {
  name: "drop6_hha"
  type: "Dropout"
  bottom: "fc6_hha"
  top: "fc6_hha"
  dropout_param {
    dropout_ratio: 0.5
  }
}
layer {
  name: "fc7_hha"
  type: "InnerProduct"
  bottom: "fc6_hha"
  top: "fc7_hha"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 4096
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu7_hha"
  type: "ReLU"
  bottom: "fc7_hha"
  top: "fc7_hha"
}
layer {
  name: "drop7_hha"
  type: "Dropout"
  bottom: "fc7_hha"
  top: "fc7_hha"
  dropout_param {
    dropout_ratio: 0.5
  }
}
layer {
  name: "concat"
  type: "Concat"
  bottom: "fc7_rgb"
  bottom: "fc7_hha"
  top: "fc7_concat"
}
layer {
  name: "fc8_svm"
  type: "InnerProduct"
  bottom: "fc7_concat"
  top: "fc8_svm"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: N_CLASSES
    weight_filler {
      type: "constant"
      value: 0
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "cls_score"
  type: "Softmax"
  bottom: "fc8_svm"
  top: "cls_score"
}
