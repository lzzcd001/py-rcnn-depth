import os
import sys
import os.path as osp


def add_path(path):
    if path not in sys.path:
        sys.path.insert(0, path)

this_dir = osp.dirname(__file__)

# Add lib to PYTHONPATH
lib_path = osp.join(this_dir, 'lib')
add_path(lib_path)

from config.uw_config import prototxt_dir, imagenet_model, snapshot_dir, merged_model_name
from lib.data_proto_gen import prepare_proto_and_window_file as ppwf
from python_utils.rcnn_depth_surgery import rcnn_depth_surgery
from tools.train_svms import train_svms

def main():
	# ppwf.generate()
	cmd = 'caffe train --gpu all --iterations 30000 --solver %s --weights %s --model %s/imagenet_deploy.prototxt 2>&1 | tee %s\n\n\n' % (
		osp.join(prototxt_dir, 'uw_finetune_solver.rgb'), imagenet_model, prototxt_dir, osp.join(prototxt_dir, 'color.log'))
	os.system(cmd)
	cmd = 'caffe train --gpu all --iterations 30000 --solver %s --weights %s --model %s/imagenet_deploy.prototxt 2>&1 | tee %s\n\n\n' % (
			osp.join(prototxt_dir, 'uw_finetune_solver.hha'), imagenet_model, prototxt_dir, osp.join(prototxt_dir, 'hha.log'))
	os.system(cmd)

	rcnn_proto = osp.join(prototxt_dir, 'uw_finetune_train.rcnn')
	rcnn_model = os.path.join(snapshot_dir,merged_model_name)

	rcnn_depth_surgery(osp.join(prototxt_dir, 'uw_finetune_train.rgb'), osp.join(prototxt_dir, 'uw_finetune_train.hha')
		, osp.join(snapshot_dir, 'uw_finetune_rgb_%d.caffemodel' % 30000), osp.join(snapshot_dir, 'uw_finetune_hha_%d.caffemodel' % 30000)
		, rcnn_proto, rcnn_model)
	train_svms('uw_rgb+hha_train', rcnn_proto, rcnn_model)
	
	#### TEST #####
	# You can use im_detect from fast_rcnn/test.py to do testing
	# <To be implemented>


	

if __name__ == "__main__":
	main()